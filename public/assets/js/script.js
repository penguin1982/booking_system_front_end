const navItems = document.getElementById('navSession');

userToken = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');

if (!userToken) {
	navItems.innerHTML = `<li class="nav-item">
        <a href="/s18-25-capstone-project/frontend/pages/login.html" class="nav-link">Log in</a>
    </li>
    <li class="nav-item">
        <a href="/s18-25-capstone-project/frontend/pages/register.html" class="nav-link">Register</a>
    </li>
    `;
}
else if (isAdmin == 'true') {
	navItems.innerHTML = `<li class="nav-item">
        <a href="/s18-25-capstone-project/frontend/pages/logout.html" class="nav-link">Log Out</a>
    </li>
    <li class="nav-item">
        <a href="/s18-25-capstone-project/frontend/pages/addCourse.html" class="nav-link">Add Course</a>
    </li>
    `;
}
else {
	navItems.innerHTML = `<li class="nav-item">
        <a href="/s18-25-capstone-project/frontend/pages/logout.html" class="nav-link">Log Out</a>
    </li>
    <li class="nav-item">
        <a href="/s18-25-capstone-project/frontend/pages/profile.html" class="nav-link">Profile</a>
    </li>
    `;
}
