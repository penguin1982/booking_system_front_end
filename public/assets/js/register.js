const formRegister = document.querySelector('#registerUser');

formRegister.addEventListener('submit', (e) => {
	const firstName = document.querySelector('#firstName').value;
	const lastName = document.querySelector('#lastName').value;
	const userEmail = document.querySelector('#userEmail').value;
	const mobileNumber = document.querySelector('#mobileNumber').value;
	const password1 = document.querySelector('#password1').value;
	const password2 = document.querySelector('#password2').value;

	const urlCheckEmail = 'https://mysterious-forest-15686.herokuapp.com/users/email-exists';
	const urlRegister = 'https://mysterious-forest-15686.herokuapp.com/users/register';

	const config = {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			email: userEmail,
			mobileNo: mobileNumber,
			password: password1
		})
	};

	e.preventDefault();

	if (password1 !== password2) {
		alert("Password does'nt match");
	}
	else if (isNaN(mobileNumber) || mobileNumber < 12) {
		alert('Invalid Mobile Number');
	}
	else {
		// const register = async () => {
		// 	const resCheckEmail = await fetch(urlCheckEmail, config);
		// 	const resEmailJSON = await resCheckEmail.json();
		// 	if (!resEmailJSON) {
		// 		const resRegister = await fetch(urlRegister, config);
		// 		const resRegisterJSON = await resRegister.json();
		// 		if (resRegisterJSON) {
		// 			alert('User Added Successfully');
		// 		} else {
		// 			alert('There is something wrong');
		// 		}
		// 	} else {
		// 		alert('Email already exist');
		// 	}
		// };

		// register();

		fetch(urlCheckEmail, config).then((res) => res.json()).then((data) => {
			if (!data) {
				fetch(urlRegister, config)
					.then((res) => {
						return res.json();
					})
					.then((data) => {
						if (data) {
							alert('New user successfully added, You can now Login');
							window.location.replace('./login.html');
						}
						else {
							alert('Something went wrong');
						}
					});
			}
			else {
				alert('Email already exist');
			}
		});
	}
});
