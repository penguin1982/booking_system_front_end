const loginForm = document.querySelector('#loginUser');

loginForm.addEventListener('submit', (e) => {
	e.preventDefault();
	const email = document.querySelector('#userEmail').value;
	console.log(email);
	const password = document.querySelector('#password').value;
	console.log(password);

	const urlLogin = 'https://mysterious-forest-15686.herokuapp.com/api/users/login';
	const urlUserDetails = 'https://mysterious-forest-15686.herokuapp.com/api/users/details';

	const config = {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			email: email,
			password: password
		})
	};

	if (email == '' || password == '') {
		alert('Please input your email and/or password');
	}
	else {
		fetch(urlLogin, config)
			.then((res) => {
				return res.json();
			})
			.then((data) => {
				if (data.access) {
					localStorage.setItem('token', data.access);
					fetch(urlUserDetails, {
						headers: {
							Authorization: `Bearer ${data.access}`
						}
					})
						.then((res) => res.json())
						.then((data) => {
							localStorage.setItem('id', data._id);
							localStorage.setItem('isAdmin', data.isAdmin);
							window.location.replace('./courses.html');
						});
				}
				else {
					alert('Something went wrong, check your credential');
				}
			});
	}
});
