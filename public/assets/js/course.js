let params = new URLSearchParams(window.location.search);
let id = params.get('courseId');
console.log(id);

let name = document.querySelector('#courseName');
let desc = document.querySelector('#courseDesc');
let price = document.querySelector('#coursePrice');
const url = `https://mysterious-forest-15686.herokuapp.com/api/courses/${id}`;

fetch(url).then((res) => res.json()).then((data) => {
	name.innerHTML = data.name;
	desc.innerHTML = data.description;
	price.innerHTML = data.price;
});
