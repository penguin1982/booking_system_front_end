const courseContainer = document.getElementById('coursesContainer');
const modalButton = document.getElementById('adminButton');

let token = localStorage.getItem('token');

const url = 'https://mysterious-forest-15686.herokuapp.com/api/courses';

// let isAdmin = localStorage.getItem('isAdmin');
let cardFooter;

// if (!isAdmin || isAdmin == 'false') {
// 	modalButton.innerHTML = null;
// }
// else {
// 	modalButton.innerHTML = `
// 	<div class="col-md2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a></div>
// `;
// }

//<p class="card-text text-left">${course.createdOn}</p>

fetch(url).then((res) => res.json()).then((data) => {
	let courseData;

	if (data.length < 1) {
		courseData = 'No course Available';
	}
	else {
		console.log(token);
		courseData = data
			.map((course) => {
				if ((isAdmin == 'false' || !isAdmin) && token !== null) {
					cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View course details</a>
					<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Enroll</a>
					`;
				}
				else if (token == null) {
					cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View course details</a>
					<a href="./login.html" class="btn btn-secondary text-white btn-block">Enroll</a>`;
				}
				else {
					cardFooter = `
					<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Edit</a>
					<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Disable Course</a>
					`;
				}

				return `
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-left">₱${course.price}</p>
								
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
					`;
			})
			.join('');
	}
	courseContainer.innerHTML = courseData;
});
