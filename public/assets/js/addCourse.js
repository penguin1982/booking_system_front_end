const createCourse = document.querySelector('#createCourse');

let token = localStorage.getItem('token');

if (!token || token === null) {
	window.location.href = './login.html';
	// alert('You must login first');
}

createCourse.addEventListener('submit', (e) => {
	const courseName = document.querySelector('#courseName').value;
	const coursePrice = document.querySelector('#coursePrice').value;
	const courseDescription = document.querySelector('#courseDescription').value;
	const courseExistURL = 'https://mysterious-forest-15686.herokuapp.com/api/courses/course-exists';
	const addCourseURL = 'https://mysterious-forest-15686.herokuapp.com/api/courses/addCourse';
	const config = {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	};

	e.preventDefault();

	fetch(courseExistURL, config).then((res) => res.json()).then((data) => {
		console.log(data);
		if (!data) {
			fetch(addCourseURL, config)
				.then((res) => {
					return res.json();
				})
				.then((data) => {
					if (data) {
						// alert('New course successfully added');
						window.location.replace('./courses.html');
					}
					else {
						alert('Something went wrong');
					}
				});
		}
		else {
			alert('Course already exist');
		}
	});
});
