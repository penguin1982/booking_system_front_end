let token = localStorage.getItem('token');
const profile = document.getElementById('profileContainer');

const urlDetails = 'https://mysterious-forest-15686.herokuapp.com/users/details';
const config = {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		Authorization: `Bearer ${token}`
	}
};

if (!token || token === null) {
	window.location.href = './login.html';
	// alert('You must login first');
}
else {
	fetch(urlDetails, config).then((res) => res.json()).then((data) => {
		console.log(data);
		profile.innerHTML = `
		<div class="col-md-12">
			<section class="jumbotron my-auto card">
				<h3 class="text-center">First Name: ${data.firstName} </h3>
				<h3 class="text-center">Last Name: ${data.lastName}</h3>
				<h3 class="text-center">Email: ${data.email}</h3>

				<table class="table mt-4">
					<thead>
						<tr>
							<th>Course ID:</th>
							<th>Enrolled On:</th>
							<th>Status:</th>
							<tbody></tbody>
						</tr>
					</thead>
				</table>

			</section>
		</div>
		`;
	});
}
